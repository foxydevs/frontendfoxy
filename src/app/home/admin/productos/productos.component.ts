import { Component, OnInit } from '@angular/core';
import { ProductosService } from "./../_services/productos.service";
import { TipoProductosService } from "./../_services/tipo-productos.service";
import { InventarioService } from "./../_services/inventario.service";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  title:string="Roles"
  Table:any
  productos:any
  comboTiposProducto:any
  idCurr=+localStorage.getItem('currentId');
  idRol=+localStorage.getItem('currentRolId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  selectedData:any
  prod:any = {
    codigo:"",
    id:0,
    descripcion:"",
    nombre:"",
    tipo:'',
    marcaDes:''
  }
  public rowsOnPage = 5;
  public search:any
  public searchterm:any
  constructor(
    private _service: NotificationsService,
    private parentService: TipoProductosService,
    private secondParentService: InventarioService,
    private mainService: ProductosService
  ) { }

  ngOnInit() {
    this.cargarAll()
    this.cargarProds()
    this.cargarCombos()
  }

  seleccionarProd(data){
    this.prod=data
    this.prod.inventario.cantidad = 0
    this.searchterm=data.codigo
  }

  cargarAll(){
    this.idCurr=+localStorage.getItem('currentId');
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll(this.idCurr)
                      .then(response => {
                        this.Table = response
                        // console.log(this.idCurr);
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }

  changeCalc(){
    this.prod.inventario.precioClienteEs    = this.prod.inventario.precioVenta-(this.prod.inventario.precioVenta*0.15)
    this.prod.inventario.precioDistribuidor = this.prod.inventario.precioVenta-(this.prod.inventario.precioVenta*0.20)
  }

  limpiarForm(){
    $('#prod-form')[0].reset()
    console.clear
    this.cargarAll()
    this.cargarProds()
    this.searchterm = ""
  }

  agregarVenta(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.subtotal = (formValue.cantidad*formValue.precioCosto)
    console.log(formValue);
    this.secondParentService.create(formValue)
                      .then(response => {
                        $('#prod-form')[0].reset()
                        $('#Loading').css('display','none')
                        console.clear
                        this.cargarProds()
                        this.searchterm = ""
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  cargarCombos(){
    this.idCurr=+localStorage.getItem('currentId');
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.parentService.getAll(this.idCurr)
                              .then(response => {
                                this.comboTiposProducto = response
                                $('#Loading').css('display','none')
                                console.clear
                              }).catch(error => {
                                console.clear
                                this.createError(error)
                                $('#Loading').css('display','none')
                              })
  }

  cargarProds(){
    this.idCurr=+localStorage.getItem('currentId');
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll(this.idCurr)
                      .then(response => {
                        this.productos = response
                        // console.log(this.idCurr);

                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }

  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }


  insertProd(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarProds()
                        this.seleccionarProd(response)
                        console.clear
                        this.create('Productos Ingresado')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }
  cargarSingle(id:number){
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea eliminar el Rol?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Rol Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      $('#Loading').css('display','none')
    }

  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}
