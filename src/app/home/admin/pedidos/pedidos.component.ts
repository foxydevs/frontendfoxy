import { Component, OnInit } from '@angular/core';
import { ModulosService } from "./../_services/modulos.service";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {
  title:string="Pedidos"
  pedidos:any=[]
  procesos:any=[]
  hechos:any=[]
  droppedItemsId:any=[]
  childs:any=[]
  childsId:any=[]
  droppedItems:any=[]
  droppedItemsId2:any=[]
  parentCombo:any
  selectedParent:any
  public rowsOnPage = 5;
  public search:any
  public searchP:any
  public searchH:any
  constructor(
    private _service: NotificationsService,
    private mainService: ModulosService,
    // private ChildsService: GradesService,
    // private ParentsService: CiclosJornadaService
  ) { }
    ngOnInit() {
      this.cargarAll()
      this.cargarFree()
    }
    DropHechos(e: any) {
        // Get the dropped data here
            // this.droppedItemsId.push({"id":e.dragData.id});
            if(e.dragData.lado==1){
              e.dragData.lado=3;
              this.hechos.push(e.dragData);
              this.pedidos.splice(this.pedidos.findIndex(dat1=>{
                return dat1.id==e.dragData.id
              }),1)
              this.search = '';
            }
            if(e.dragData.lado==2){
              e.dragData.lado=3;
              this.hechos.push(e.dragData);
              this.procesos.splice(this.procesos.findIndex(dat=>{
                return dat.id==e.dragData.id
              }),1)
              this.searchP = '';
            }
    }
    DropProcesos(e: any) {
        // Get the dropped data here
            // this.droppedItemsId.push({"id":e.dragData.id});
            if(e.dragData.lado==1){
              e.dragData.lado=2;
              this.procesos.push(e.dragData);
              this.pedidos.splice(this.pedidos.findIndex(dat=>{
                return dat.id==e.dragData.id
              }),1)
              this.search = '';
            }

            if(e.dragData.lado==3){
              e.dragData.lado=2;
              this.procesos.push(e.dragData);
              this.hechos.splice(this.hechos.findIndex(dat1=>{
                return dat1.id==e.dragData.id
              }),1)
              this.searchH = '';
            }


    }
    DropPedidos(e: any) {
        // Get the dropped data here
            // this.droppedItemsId.push({"id":e.dragData.id});
            if(e.dragData.lado==2){
              e.dragData.lado=1;
              this.pedidos.push(e.dragData);
              this.procesos.splice(this.procesos.findIndex(dat=>{
                return dat.id==e.dragData.id
              }),1)
              this.searchP = '';
            }

            if(e.dragData.lado==3){
              e.dragData.lado=1;
              this.pedidos.push(e.dragData);
              this.hechos.splice(this.hechos.findIndex(dat1=>{
                return dat1.id==e.dragData.id
              }),1)
              this.searchH = '';
            }



    }
    cargarFree(){
      // this.childsId.length = 0;
      // this.ChildsService.getAll()
      //                   .then(response => {
      //                     this.childs = response
      //                     this.childs.forEach((item,index)=>{
      //                       this.childsId.push({"id":item.id});
      //                     })
      //                     console.clear
      //                   }).catch(error => {
      //                     console.clear
      //                     this.createError(error)
      //                   })
    }
    cargarAll(){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.getAll()
                        .then(response => {
                          response.forEach(element => {
                            element.lado=1
                          });
                          this.pedidos = response
                          $('#Loading').css('display','none')
                          console.clear
                        }).catch(error => {
                          console.clear
                          $('#Loading').css('display','none')
                          this.createError(error)
                        })
    }
    limpiar(){
      // this.selectedData.length=0
      this.droppedItemsId.length=0
      this.droppedItems.length=0
      this.selectedParent=null
    }
    cargarSingle(id:number){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.selectedParent=id
      this.droppedItemsId.length = 0;
      this.childsId.length = 0;
      this.cargarFree()
      // this.mainService.getMyChilds(id)
      //                   .then(response => {
      //                     this.selectedData = response
      //                     this.selectedData.forEach((item,index)=>{
      //                       this.droppedItemsId.push({"id":item.id});
      //                     })
      //                     $('#Loading').css('display','none')
      //                     console.clear

      //                   }).catch(error => {
      //                     console.clear
      //                     this.createError(error)
      //                   })
    }
    update(formValue:any){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      //console.log(data)
      this.mainService.update(formValue)
                        .then(response => {
                          $("#editModal .close").click();
                          this.cargarAll()
                          console.clear
                          this.create('Ciclo Actualizado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })

    }
    delete2(id:string){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Grados Desasignados')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })

    }
    delete(formValueDel){
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')

      this.droppedItemsId.forEach(element => {
        this.childsId.splice(this.childsId.findIndex(dat => {
          return dat.id==element.id
        }),1)
      })



      let formValue = {
        "master":this.selectedParent,
        "grades": this.droppedItemsId
      }

      formValueDel = {
        "master":this.selectedParent,
        "grades": this.childsId
      }
      //  console.log(formValue);
      //  console.log(formValueDel);


      if(this.selectedParent){
      // this.mainService.deleteAll(formValueDel)
      //                   .then(response => {
      //                     this.insert(formValue)
      //                     this.create('Grados Desasignados')
      //                   }).catch(error => {
      //                     this.insert(formValue)
      //                     console.clear
      //                     this.createError(error)
      //                   })
                      }else{
                        this.createError("Debe seleccionar un Ciclo-Jornada")
                      }

    }
    insert(formValue:any){

      this.mainService.create(formValue)
                        .then(response => {
                          $("#insertModal .close").click();
                          this.cargarAll()
                          console.clear
                          this.create('Grados Asignados')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })



    }

  public options = {
               position: ["bottom", "right"],
               timeOut: 2000,
               lastOnBottom: false,
               animate: "fromLeft",
               showProgressBar: false,
               pauseOnHover: true,
               clickToClose: true,
               maxLength: 200
           };

    create(success) {
                this._service.success('¡Éxito!',success)

    }
    createError(error) {
                this._service.error('¡Error!',error)

    }
}
