import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from "./admin.component";

import { PerfilComponent } from './perfil/perfil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RolesComponent } from './roles/roles.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { ModulosComponent } from './modulos/modulos.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { ClientesComponent } from './clientes/clientes.component';
import { EstadisticaComponent } from './estadistica/estadistica.component';
import { UsuariosExternosComponent } from './usuarios-externos/usuarios-externos.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { ProductosComponent } from './productos/productos.component';
import { ComprasComponent } from './compras/compras.component';
import { GenerarCompraComponent } from './generar-compra/generar-compra.component';
import { InventarioComponent } from './inventario/inventario.component';
import { TipoProductosComponent } from './tipo-productos/tipo-productos.component';
import { PedidosComponent } from './pedidos/pedidos.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'roles', component: RolesComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'usuarios-externos', component: UsuariosExternosComponent },
    { path: 'modulos', component: ModulosComponent },
    { path: 'empleados', component: EmpleadosComponent },
    { path: 'clientes', component: ClientesComponent },
    { path: 'proveedores', component: ProveedoresComponent },
    { path: 'productos', component: ProductosComponent },
    { path: 'compras', component: ComprasComponent },
    { path: 'generar-compra', component: GenerarCompraComponent },
    { path: 'inventario', component: InventarioComponent },
    { path: 'categorias', component: TipoProductosComponent },
    { path: 'estadistica', component: EstadisticaComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: 'sucursales', component: SucursalesComponent },
    { path: 'pedidos', component: PedidosComponent }
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
