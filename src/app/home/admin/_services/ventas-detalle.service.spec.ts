import { TestBed, inject } from '@angular/core/testing';

import { VentasDetalleService } from './ventas-detalle.service';

describe('VentasDetalleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VentasDetalleService]
    });
  });

  it('should be created', inject([VentasDetalleService], (service: VentasDetalleService) => {
    expect(service).toBeTruthy();
  }));
});
