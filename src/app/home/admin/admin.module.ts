import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { AdminRoutingModule } from './admin.routing';

import { EmployeesService } from './_services/employees.service';
import { ModulosService } from './_services/modulos.service';
import { SucursalesService } from './_services/sucursales.service';
import { ClientesService } from './_services/clientes.service';
import { ProveedoresService } from './_services/proveedores.service';
import { AccesosService } from './_services/accesos.service';
import { ComprasService } from './_services/compras.service';
import { InventarioService } from './_services/inventario.service';
import { ProductosService } from './_services/productos.service';
import { TipoProductosService } from './_services/tipo-productos.service';

import { AdminComponent } from './admin.component';
import { LoaderComponent } from './loader/loader.component';
import { PerfilComponent } from './perfil/perfil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RolesComponent } from './roles/roles.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { ModulosComponent } from './modulos/modulos.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { ClientesComponent } from './clientes/clientes.component';
import { EstadisticaComponent } from './estadistica/estadistica.component';
import { UsuariosExternosComponent } from './usuarios-externos/usuarios-externos.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { ProductosComponent } from './productos/productos.component';
import { TipoProductosComponent } from './tipo-productos/tipo-productos.component';
import { ComprasComponent } from './compras/compras.component';
import { InventarioComponent } from './inventario/inventario.component';
import { GenerarCompraComponent } from './generar-compra/generar-compra.component';
import { PedidosComponent } from './pedidos/pedidos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2DragDropModule.forRoot(),
    LoadersCssModule,
    AngularMultiSelectModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    LoaderComponent,
    PerfilComponent,
    DashboardComponent,
    RolesComponent,
    UsuariosComponent,
    SucursalesComponent,
    ModulosComponent,
    EmpleadosComponent,
    ClientesComponent,
    EstadisticaComponent,
    UsuariosExternosComponent,
    ProveedoresComponent,
    ProductosComponent,
    TipoProductosComponent,
    ComprasComponent,
    InventarioComponent,
    GenerarCompraComponent,
    PedidosComponent,
  ],
  providers: [
    EmployeesService,
    ModulosService,
    AccesosService,
    InventarioService,
    SucursalesService,
    ComprasService,
    ProductosService,
    ClientesService,
    ProveedoresService,
    TipoProductosService
  ]
})
export class AdminModule { }
